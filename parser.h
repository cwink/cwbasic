#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>

#include "main.h"

enum token_type {
	TOKEN_TYPE_NONE = 0x0U,

	TOKEN_TYPE_COMMA = 0x1U,
	TOKEN_TYPE_END_OF_FILE = 0x2U,
	TOKEN_TYPE_NEWLINE = 0x3U,
	TOKEN_TYPE_NUMBER = 0x4U,
	TOKEN_TYPE_STRING = 0x5U,
	TOKEN_TYPE_VARIABLE = 0x6U,

	TOKEN_TYPE_ASTERISK = 0x10U,
	TOKEN_TYPE_EQUAL = 0x20U,
	TOKEN_TYPE_GREATER_THAN = 0x30U,
	TOKEN_TYPE_GREATER_THAN_OR_EQUAL = 0x40U,
	TOKEN_TYPE_GROUP_CLOSE = 0x50U,
	TOKEN_TYPE_GROUP_OPEN = 0x60U,
	TOKEN_TYPE_LESS_THAN = 0x70U,
	TOKEN_TYPE_LESS_THAN_OR_EQUAL = 0x80U,
	TOKEN_TYPE_MINUS = 0x90U,
	TOKEN_TYPE_NOT_EQUAL = 0xA0U,
	TOKEN_TYPE_PLUS = 0xB0U,
	TOKEN_TYPE_SLASH = 0xC0U,

	TOKEN_TYPE_CLEAR = 0x100U,
	TOKEN_TYPE_END = 0x200U,
	TOKEN_TYPE_GOSUB = 0x300U,
	TOKEN_TYPE_GOTO = 0x400U,
	TOKEN_TYPE_IF = 0x500U,
	TOKEN_TYPE_INPUT = 0x600U,
	TOKEN_TYPE_LET = 0x700U,
	TOKEN_TYPE_LIST = 0x800U,
	TOKEN_TYPE_PRINT = 0x900U,
	TOKEN_TYPE_RETURN = 0xA00U,
	TOKEN_TYPE_RUN = 0xB00U,
	TOKEN_TYPE_THEN = 0xC00U
};

#define LEXER_COLUMN(LEXER) ((LEXER).code - (LEXER).line_begin + 1)

struct ALIGNED(16) token {
	enum token_type type;
	char *value;
};

struct ALIGNED(32) lexer {
	size_t line;
	char *code, *line_begin;
};

struct ALIGNED(64) parser {
	struct lexer lexer;
	struct token token;
};

void lexer_get_token(struct lexer *lexer, struct token *token);
void lexer_unget_token(struct lexer *lexer);
void parser_initialize(struct parser *parser, char *code);
void parser_run(struct parser *parser);
void token_dump(FILE *file, const struct token *token);

#endif /* PARSER_H */
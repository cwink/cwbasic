.POSIX:
.SUFFIXES:

CC      := gcc
#CFLAGS  := -std=c99 -Os -DNDEBUG -D_POSIX_C_SOURCE=200809L
CFLAGS  := -std=c99 -Wall -Wextra -pedantic-errors -O0 -g \
           -D_POSIX_C_SOURCE=200809L
LDFLAGS := -static
LDLIBS  :=

all: main.x

main.x: main.o die.o is_newline.o lexer_get_token.o parser_initialize.o \
        parser_run.o token_dump.o

clean:
	rm -rf *.x *.o

.SUFFIXES: .c .o .x
.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.o.x:
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) 
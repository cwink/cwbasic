#include "main.h"

int
is_newline(const char *const string)
{
#if NEWLINE_LENGTH == 1
	return *string == '\n';
#else
	return *string == '\r' && *(string + 1) == '\n';
#endif /* NEWLINE_LENGTH == 1 */
}
#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"

static char *read_file(const char *filename);

static char *
read_file(const char *const filename)
{
	assert(filename != NULL);
	FILE *file;
	long file_size;
	char *buffer;
	if ((file = fopen(filename, "r")) == NULL)
		die("Failed to open file '%s'.", filename);
	if (fseek(file, 0, SEEK_END) == -1)
		die("Failed to seek to end of file '%s'.", filename);
	if ((file_size = ftell(file)) == -1)
		die("Failed to get size of file '%s'.", filename);
	if (fseek(file, 0, SEEK_SET) == -1)
		die("Failed to seek to end of file '%s'.", filename);
	if ((buffer = malloc(sizeof(char) * file_size)) == NULL)
		die("Failed to allocate buffer for contents of file '%s'.",
		    filename);
	if (fread(buffer, sizeof(char), file_size, file) < (size_t)file_size)
		die("Failed to read code into buffer for file '%s'.", filename);
	(void)fclose(file);
	return buffer;
}

int
main(int argc, char *argv[])
{
	struct parser parser;
	char *code;
	if (argc != 2) {
		printf("Usage: ./main.x [program]\n");
		printf("\n\tprogram - The basic program to run.\n");
		return EXIT_SUCCESS;
	}
	code = read_file(argv[1]);
	parser_initialize(&parser, code);
	parser_run(&parser);
	free(code);
	return EXIT_SUCCESS;
}
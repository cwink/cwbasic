#include <assert.h>

#include "parser.h"

static int
parser_parse_factor(struct parser *const parser, int *const result)
{
	assert(parser != NULL && result != NULL);
}

static int
parser_parse_term(struct parser *const parser, int *const result)
{
	int left, right;
	assert(parser != NULL && result != NULL);
	left = parser_parse_factor(parser, result);
}

static int
parser_parse_expression(struct parser *const parser, int *const result)
{
	int left, multiplier, right;
	assert(parser != NULL && result != NULL);
	multiplier = 1;
	if (parser->token.type == TOKEN_TYPE_MINUS ||
	    parser->token.type == TOKEN_TYPE_PLUS) {
		if (parser->token.type == TOKEN_TYPE_MINUS)
			multiplier *= -1;
		lexer_get_token(&parser->lexer, &parser->token);
	}
	parser_parse_term(parser, &left);
	parser_parse_term(parser, &right);
}

static void
parser_parse_print(struct parser *const parser)
{
	int result;
	assert(parser != NULL);
	lexer_get_token(&parser->lexer, &parser->token);
	if (parser->token.type == TOKEN_TYPE_STRING) {
		printf("%s\n", parser->token.value);
	} else {
		if (parser_parse_expression(parser, &result) < 0)
			die("Expected expression on line %zu column %zu.",
			    parser->lexer.line, LEXER_COLUMN(parser->lexer));
		else
			printf("%d", result);
	}
	while (1) {
		lexer_get_token(&parser->lexer, &parser->token);
		if (parser->token.type == TOKEN_TYPE_STRING) {
			printf("        %s\n", parser->token.value);
		} else {
			if (parser_parse_expression(parser, &result) < 0)
				break;
			else
				printf("        %d", result);
		}
	}
}

static void
parser_parse_statement(struct parser *const parser)
{
	assert(parser != NULL);
	lexer_get_token(&parser->lexer, &parser->token);
	switch (parser->token.type) {
	case TOKEN_TYPE_PRINT:
		parser_parse_print(parser);
		break;
	case TOKEN_TYPE_IF:
		break;
	case TOKEN_TYPE_GOTO:
		break;
	case TOKEN_TYPE_INPUT:
		break;
	case TOKEN_TYPE_LET:
		break;
	case TOKEN_TYPE_GOSUB:
		break;
	case TOKEN_TYPE_RETURN:
		break;
	case TOKEN_TYPE_CLEAR:
		break;
	case TOKEN_TYPE_LIST:
		break;
	case TOKEN_TYPE_RUN:
		break;
	case TOKEN_TYPE_END:
		break;
	default:
		die("Unexpected token encountered on line %zu column %zu.",
		    parser->lexer.line, LEXER_COLUMN(parser->lexer));
	}
	if (parser->token.type != TOKEN_TYPE_NEWLINE)
		die("Expected newline character on line %zu column %zu.",
		    parser->lexer.line, LEXER_COLUMN(parser->lexer));
}

void
parser_run(struct parser *const parser)
{
	assert(parser != NULL);
	do {
		lexer_get_token(&parser->lexer, &parser->token);
		token_dump(stdout, &parser->token);
		printf("\n");
	} while (parser->token.type != TOKEN_TYPE_END_OF_FILE);
}
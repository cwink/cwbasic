#include <assert.h>
#include <ctype.h>
#include <string.h>

#include "parser.h"

void
lexer_get_token(struct lexer *const lexer, struct token *const token)
{
	assert(lexer != NULL && lexer->code != NULL && token != NULL);
	token->type = TOKEN_TYPE_NONE;
	token->value = NULL;
	for (; !is_newline(lexer->code) && isspace(*lexer->code) != 0;
	     ++lexer->code)
		continue;
	if (is_newline(lexer->code)) {
		++lexer->line;
		lexer->code += NEWLINE_LENGTH;
		lexer->line_begin = lexer->code;
		token->type = TOKEN_TYPE_NEWLINE;
		return;
	}
	switch (*lexer->code) {
	case '\0':
		token->type = TOKEN_TYPE_END_OF_FILE;
		return;
	case '+':
		token->type = TOKEN_TYPE_PLUS;
		++lexer->code;
		return;
	case '-':
		token->type = TOKEN_TYPE_MINUS;
		++lexer->code;
		return;
	case '*':
		token->type = TOKEN_TYPE_ASTERISK;
		++lexer->code;
		return;
	case '/':
		token->type = TOKEN_TYPE_SLASH;
		++lexer->code;
		return;
	case '<':
		if (*(lexer->code + 1) == '=') {
			token->type = TOKEN_TYPE_LESS_THAN_OR_EQUAL;
			++lexer->code;
		} else if (*(lexer->code + 1) == '>') {
			token->type = TOKEN_TYPE_NOT_EQUAL;
			++lexer->code;
		} else {
			token->type = TOKEN_TYPE_LESS_THAN;
		}
		++lexer->code;
		return;
	case '>':
		if (*(lexer->code + 1) == '=') {
			token->type = TOKEN_TYPE_GREATER_THAN_OR_EQUAL;
			++lexer->code;
		} else if (*(lexer->code + 1) == '<') {
			token->type = TOKEN_TYPE_NOT_EQUAL;
			++lexer->code;
		} else {
			token->type = TOKEN_TYPE_GREATER_THAN;
		}
		++lexer->code;
		return;
	case '=':
		token->type = TOKEN_TYPE_EQUAL;
		++lexer->code;
		return;
	case '(':
		token->type = TOKEN_TYPE_GROUP_OPEN;
		++lexer->code;
		return;
	case ')':
		token->type = TOKEN_TYPE_GROUP_CLOSE;
		++lexer->code;
		return;
	case ',':
		token->type = TOKEN_TYPE_COMMA;
		++lexer->code;
		return;
	case '"':
		token->type = TOKEN_TYPE_STRING;
		token->value = ++lexer->code;
		for (; *lexer->code != '"'; ++lexer->code)
			if (*lexer->code < 0x20 || *lexer->code > 0x7E)
				die("Invalid string character '%c' encountered "
				    "on line %zu column %zu.",
				    *lexer->code, lexer->line,
				    LEXER_COLUMN(*lexer));
		*lexer->code = '\0';
		++lexer->code;
		return;
	}
	if (isdigit(*lexer->code) != 0) {
		token->type = TOKEN_TYPE_NUMBER;
		token->value = lexer->code;
		for (; isdigit(*lexer->code) != 0; ++lexer->code)
			continue;
		*lexer->code = '\0';
		++lexer->code;
		return;
	}
	if (*(lexer->code + 1) != '\0') {
		if (memcmp(lexer->code, "IF", 2) == 0) {
			token->type = TOKEN_TYPE_IF;
			lexer->code += 2;
			return;
		}
	}
	if (*(lexer->code + 2) != '\0') {
		if (memcmp(lexer->code, "END", 3) == 0) {
			token->type = TOKEN_TYPE_END;
			lexer->code += 3;
			return;
		}
		if (memcmp(lexer->code, "LET", 3) == 0) {
			token->type = TOKEN_TYPE_LET;
			lexer->code += 3;
			return;
		}
		if (memcmp(lexer->code, "RUN", 3) == 0) {
			token->type = TOKEN_TYPE_RUN;
			lexer->code += 3;
			return;
		}
	}
	if (*(lexer->code + 3) != '\0') {
		if (memcmp(lexer->code, "GOTO", 4) == 0) {
			token->type = TOKEN_TYPE_GOTO;
			lexer->code += 4;
			return;
		}
		if (memcmp(lexer->code, "LIST", 4) == 0) {
			token->type = TOKEN_TYPE_LIST;
			lexer->code += 4;
			return;
		}
		if (memcmp(lexer->code, "THEN", 4) == 0) {
			token->type = TOKEN_TYPE_THEN;
			lexer->code += 4;
			return;
		}
	}
	if (*(lexer->code + 4) != '\0') {
		if (memcmp(lexer->code, "CLEAR", 5) == 0) {
			token->type = TOKEN_TYPE_CLEAR;
			lexer->code += 5;
			return;
		}
		if (memcmp(lexer->code, "GOSUB", 5) == 0) {
			token->type = TOKEN_TYPE_GOSUB;
			lexer->code += 5;
			return;
		}
		if (memcmp(lexer->code, "INPUT", 5) == 0) {
			token->type = TOKEN_TYPE_INPUT;
			lexer->code += 5;
			return;
		}
		if (memcmp(lexer->code, "PRINT", 5) == 0) {
			token->type = TOKEN_TYPE_PRINT;
			lexer->code += 5;
			return;
		}
	}
	if (*(lexer->code + 5) != '\0') {
		if (memcmp(lexer->code, "RETURN", 6) == 0) {
			token->type = TOKEN_TYPE_RETURN;
			lexer->code += 6;
			return;
		}
	}
	if (isalpha(*lexer->code) != 0) {
		token->type = TOKEN_TYPE_VARIABLE;
		token->value = lexer->code;
		++lexer->code;
		return;
	}
	die("Invalid token '%c' encountered on line %zu column %zu.",
	    *lexer->code, lexer->line, LEXER_COLUMN(*lexer));
}

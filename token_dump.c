#include <assert.h>

#include "parser.h"

void
token_dump(FILE *const file, const struct token *const token)
{
	assert(token != NULL);
	switch (token->type) {
	case TOKEN_TYPE_NONE:
		(void)fprintf(file, "NONE");
		break;
        case TOKEN_TYPE_COMMA:
                (void)fprintf(file, "COMMA");
                break;
	case TOKEN_TYPE_END_OF_FILE:
		(void)fprintf(file, "END_OF_FILE");
		break;
	case TOKEN_TYPE_NEWLINE:
		(void)fprintf(file, "NEWLINE");
		break;
	case TOKEN_TYPE_NUMBER:
		(void)fprintf(file, "NUMBER:%s", token->value);
		break;
	case TOKEN_TYPE_STRING:
		(void)fprintf(file, "STRING:\"%s\"", token->value);
		break;
	case TOKEN_TYPE_VARIABLE:
		(void)fprintf(file, "VARIABLE:'%c'", *token->value);
		break;
	case TOKEN_TYPE_ASTERISK:
		(void)fprintf(file, "ASTERISK");
		break;
	case TOKEN_TYPE_EQUAL:
		(void)fprintf(file, "EQUAL");
		break;
	case TOKEN_TYPE_GREATER_THAN:
		(void)fprintf(file, "GREATER_THAN");
		break;
	case TOKEN_TYPE_GREATER_THAN_OR_EQUAL:
		(void)fprintf(file, "GREATER_THAN_OR_EQUAL");
		break;
	case TOKEN_TYPE_GROUP_CLOSE:
		(void)fprintf(file, "GROUP_CLOSE");
		break;
	case TOKEN_TYPE_GROUP_OPEN:
		(void)fprintf(file, "GROUP_OPEN");
		break;
	case TOKEN_TYPE_LESS_THAN:
		(void)fprintf(file, "LESS_THAN");
		break;
	case TOKEN_TYPE_LESS_THAN_OR_EQUAL:
		(void)fprintf(file, "LESS_THAN_OR_EQUAL");
		break;
	case TOKEN_TYPE_MINUS:
		(void)fprintf(file, "MINUS");
		break;
	case TOKEN_TYPE_NOT_EQUAL:
		(void)fprintf(file, "NOT_EQUAL");
		break;
	case TOKEN_TYPE_PLUS:
		(void)fprintf(file, "PLUS");
		break;
	case TOKEN_TYPE_SLASH:
		(void)fprintf(file, "SLASH");
		break;
	case TOKEN_TYPE_CLEAR:
		(void)fprintf(file, "CLEAR");
		break;
	case TOKEN_TYPE_END:
		(void)fprintf(file, "END");
		break;
	case TOKEN_TYPE_GOSUB:
		(void)fprintf(file, "GOSUB");
		break;
	case TOKEN_TYPE_GOTO:
		(void)fprintf(file, "GOTO");
		break;
	case TOKEN_TYPE_IF:
		(void)fprintf(file, "IF");
		break;
	case TOKEN_TYPE_INPUT:
		(void)fprintf(file, "INPUT");
		break;
	case TOKEN_TYPE_LET:
		(void)fprintf(file, "LET");
		break;
	case TOKEN_TYPE_LIST:
		(void)fprintf(file, "LIST");
		break;
	case TOKEN_TYPE_PRINT:
		(void)fprintf(file, "PRINT");
		break;
	case TOKEN_TYPE_RETURN:
		(void)fprintf(file, "RETURN");
		break;
	case TOKEN_TYPE_RUN:
		(void)fprintf(file, "RUN");
		break;
	case TOKEN_TYPE_THEN:
		(void)fprintf(file, "THEN");
		break;
	default:
		(void)fprintf(file, "UNKNOWN");
		break;
	}
}